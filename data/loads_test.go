package data

import (
	"bytes"
	"encoding/json"
	"net/http"
	"os"
	"path/filepath"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
)

func TestShouldProcessLoadPass(t *testing.T) {
	setup()
	limit := Limit{"123", 0, 0, 0}
	res := shouldProcessLoad(limit, 100)
	assert.Equal(t, true, res, "true is expected")
}

func TestShouldProcessLoadFailDaily(t *testing.T) {
	setup()
	limit := Limit{"123", 4000, 4000, 1}
	res := shouldProcessLoad(limit, 1001)
	assert.Equal(t, false, res, "false is expected")
}

func TestShouldProcessLoadFailWeekly(t *testing.T) {
	setup()
	limit := Limit{"123", 0, 19000, 1}
	res := shouldProcessLoad(limit, 1001)
	assert.Equal(t, false, res, "false is expected")
}

func TestShouldProcessLoadFailDailyLoads(t *testing.T) {
	setup()
	limit := Limit{"123", 500, 500, 3}
	res := shouldProcessLoad(limit, 1001)
	assert.Equal(t, false, res, "false is expected")
}

func TestUpdateLimitSuccess(t *testing.T) {
	setup()
	load := Load{"123", "123", "$100", time.Now()}
	limit := Limit{"123", 0, 0, 0}
	loadAmountF := float64(100)

	expected := Limit{"123", 100, 100, 1}
	updateLimitSuccess(load, limit, loadAmountF)
	assert.Equal(t, expected, limitStore["123"], "Updated limit should equal expected limit")
}

func TestUpdateLimitDecline(t *testing.T) {
	setup()
	load := Load{"123", "123", "$100000", time.Now()}
	limit := Limit{"123", 0, 0, 0}

	expected := Limit{"123", 0, 0, 1}
	updateLimitDecline(load, limit)
	assert.Equal(t, expected, limitStore["123"], "Updated limit should equal expected limit")
}

func TestUpdateProcessedStore(t *testing.T) {
	setup()
	load := Load{"321", "321", "$100", time.Now()}
	expected := ProcessedLoad{"321", "321", true}
	updateProcessedStore(load, true)
	assert.Equal(t, expected, processedStore[0], "entry in processed store should equal expected")
}

func TestResetWeeklyLimit(t *testing.T) {
	setup()
	//Load date is a monday.
	load := Load{"123", "123", "$100", time.Date(2000, 1, 3, 0, 0, 0, 0, time.UTC)}
	limit := Limit{"123", 0, 0, 0}
	loadAmountF := float64(100)

	updateLimitSuccess(load, limit, loadAmountF)
	resetWeeklyLimits(load)
	expected := Limit{"123", 100, 0, 1}
	assert.Equal(t, expected, limitStore["123"], "Updated limit should equal expected limit")
}

func TestResetDailyLimit(t *testing.T) {
	setup()
	//Load date is a sunday.
	load := Load{"123", "123", "$100", time.Date(2000, 1, 2, 0, 0, 0, 0, time.UTC)}
	limit := Limit{"123", 0, 0, 0}
	loadAmountF := float64(100)
	updateLimitSuccess(load, limit, loadAmountF)

	resetDailyLimits(load, 24)
	expected := Limit{"123", 0, 100, 0}
	assert.Equal(t, expected, limitStore["123"], "Updated limit should equal expected limit")
}

func TestCheckforLimitReset(t *testing.T) {
	setup()
	//Load date is a sunday.
	load := Load{"123", "123", "$100", time.Date(2000, 1, 2, 0, 0, 0, 0, time.UTC)}
	limit := Limit{"123", 0, 0, 0}
	loadAmountF := float64(100)
	updateLimitSuccess(load, limit, loadAmountF)
	limitDate = time.Date(2000, 1, 1, 0, 0, 0, 0, time.UTC)

	checkForLimitReset(load)
	expected := Limit{"123", 0, 100, 0}
	assert.Equal(t, expected, limitStore["123"], "Updated limit should equal expected limit")
}

func TestCheckforLimitResetNoReset(t *testing.T) {
	setup()
	//Load date is a sunday.
	load := Load{"123", "123", "$100", time.Date(2000, 1, 2, 0, 0, 0, 0, time.UTC)}
	limit := Limit{"123", 0, 0, 0}
	loadAmountF := float64(100)
	updateLimitSuccess(load, limit, loadAmountF)
	limitDate = time.Date(2000, 1, 2, 0, 0, 0, 0, time.UTC)

	checkForLimitReset(load)
	expected := Limit{"123", 100, 100, 1}
	assert.Equal(t, expected, limitStore["123"], "Updated limit should equal expected limit")
}

func TestProcessLoad(t *testing.T) {
	setup()
	load := Load{"123", "123", "$100", time.Date(2000, 1, 2, 0, 0, 0, 0, time.UTC)}

	ProcessLoad(load)
	expectedLimit := Limit{"123", 100, 100, 1}
	expectedProcessed := ProcessedLoad{"123", "123", true}
	assert.Equal(t, expectedLimit, limitStore["123"], "New limit for 123 should equal expected limit")
	assert.Equal(t, load, loadStore["123123"], "Load entry for 123123 should equal the given load")
	assert.Equal(t, expectedProcessed, processedStore[0], "First entry in processedStore should equal expected")
}

func TestProcessLoad2Loads(t *testing.T) {
	setup()
	load := Load{"123", "123", "$100", time.Date(2000, 1, 2, 0, 0, 0, 0, time.UTC)}
	load2 := Load{"321", "123", "$200", time.Date(2000, 1, 2, 0, 0, 0, 0, time.UTC)}

	// Process 2 loads
	ProcessLoad(load)
	ProcessLoad(load2)
	expectedLimit := Limit{"123", 300, 300, 2}
	expectedProcessed := ProcessedLoad{"123", "123", true}
	expectedProcessed2 := ProcessedLoad{"321", "123", true}
	assert.Equal(t, expectedLimit, limitStore["123"], "New limit for 123 should equal expected limit")
	assert.Equal(t, load, loadStore["123123"], "Load entry for 123123 should equal the given load")
	assert.Equal(t, load2, loadStore["321123"], "Load entry for 321123 should equal the given load")
	assert.Equal(t, expectedProcessed, processedStore[0], "First entry in processedStore should equal expected")
	assert.Equal(t, expectedProcessed2, processedStore[1], "Second entry in processedStore should equal expected")
}

func TestProcessLoads(t *testing.T) {
	setup()
	loads := []string{"{\"id\":\"15887\",\"customer_id\":\"528\",\"load_amount\":\"$3318\",\"time\":\"2000-01-01T00:00:00Z\"}",
		"{\"id\":\"15889\",\"customer_id\":\"528\",\"load_amount\":\"$100\",\"time\":\"2000-01-01T00:00:00Z\"}"}

	// Process 2 loads
	ProcessLoads(loads)
	expectedLimit := Limit{"528", 3418, 3418, 2}
	expectedProcessed := ProcessedLoad{"15887", "528", true}
	expectedProcessed2 := ProcessedLoad{"15889", "528", true}
	assert.Equal(t, expectedLimit, limitStore["528"], "New limit for 528 should equal expected limit")
	assert.Equal(t, expectedProcessed, processedStore[0], "First entry in processedStore should equal expected")
	assert.Equal(t, expectedProcessed2, processedStore[1], "Second entry in processedStore should equal expected")
}

func TestReadLoads(t *testing.T) {
	setup()
	filename, _ := filepath.Abs("../input.txt")
	resp := struct {
		Filename string `json:"filename"`
	}{filename}
	byteResp, _ := json.Marshal(resp)
	req, _ := http.NewRequest("POST", "/api/loads/process", bytes.NewBuffer(byteResp))
	loadsSlice := ReadLoads(req)
	assert.Equal(t, true, len(loadsSlice) > 0, "Expect loadSlice to have a length greater than 0")
}

func TestReadLoadsWrongFile(t *testing.T) {
	filename, _ := filepath.Abs("../missing.txt")
	resp := struct {
		Filename string `json:"filename"`
	}{filename}
	byteResp, _ := json.Marshal(resp)
	req, _ := http.NewRequest("POST", "/api/loads/process", bytes.NewBuffer(byteResp))
	loadsSlice := ReadLoads(req)
	assert.Equal(t, true, len(loadsSlice) == 0, "Expect loadSlice to have a length of 0")
}

func TestWriteLoads(t *testing.T) {
	setup()
	loads := []string{"{\"id\":\"15887\",\"customer_id\":\"528\",\"load_amount\":\"$3318\",\"time\":\"2000-01-01T00:00:00Z\"}",
		"{\"id\":\"15889\",\"customer_id\":\"528\",\"load_amount\":\"$100\",\"time\":\"2000-01-01T00:00:00Z\"}"}

	// Process 2 loads
	ProcessLoads(loads)
	WriteProcessedLoads("test.txt")
	absPathOpen, _ := filepath.Abs("test.txt")
	_, err := os.Stat(absPathOpen)
	assert.Equal(t, nil, err, "Opening test.txt should not throw err")
	os.Remove(absPathOpen)
}

func setup() {
	limitStore = make(map[string]Limit)
	loadStore = make(map[string]Load)
	processedStore = processedStore[:0]
}
