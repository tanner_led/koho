package data

import (
	"bufio"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"os"
	"path/filepath"
	"strconv"
	"time"
	"unicode/utf8"
)

// Limit consts
const (
	DailyLimit  = 5000
	WeeklyLimit = 20000
	DailyLoads  = 3
)

// ProcessReq request body for ProcessLoadsHandler
type ProcessReq struct {
	Filename string `json:"filename"`
}

// Load is a single line the the input.txt batch
type Load struct {
	ID         string    `json:"id"`
	CustomerID string    `json:"customer_id"`
	LoadAmount string    `json:"load_amount"`
	Time       time.Time `json:"time"`
}

// ProcessedLoad is a single line the the input.txt batch
type ProcessedLoad struct {
	ID         string `json:"id"`
	CustomerID string `json:"customer_id"`
	Accepted   bool   `json:"accepted"`
}

// Limit is used to keep track of a users limits
type Limit struct {
	CustomerID  string  `json:"customer_id"`
	DailyLimit  float64 `json:"daily_limit"`
	WeeklyLimit float64 `json:"weekly_limit"`
	DailyLoads  float64 `json:"daily_loads"`
}

// Loads is a collection of Load
type Loads map[string]Load

// ProcessedLoads is a collection of ProcessedLoad
type ProcessedLoads []ProcessedLoad

// Limits is a collection of Limit
type Limits map[string]Limit

// Data store for the loads being read. Ideally this would be a db instead.
var limitStore Limits

// LoadStore is a data store for the loads being read. Ideally this would be a db instead.
var loadStore Loads

// Data store for the processed loads. Ideally this would be a db instead.
var processedStore ProcessedLoads

// limitDate is used to hold the date of the previous load to compare for limit resets.
var limitDate time.Time

// Init stores.
func init() {
	limitStore = make(map[string]Limit)
	loadStore = make(map[string]Load)
}

// GetLoads returns loadList.
func GetLoads() Loads {
	return loadStore
}

// GetProcessedLoads returns processedoadList.
func GetProcessedLoads() ProcessedLoads {
	return processedStore
}

// ReadLoads opens a file from the filename in the req body and reads it into a string array.
func ReadLoads(req *http.Request) []string {
	var processReq ProcessReq
	_ = json.NewDecoder(req.Body).Decode(&processReq)

	absPathOpen, _ := filepath.Abs(processReq.Filename)
	file, err := os.Open(absPathOpen)
	if err != nil {
		log.Printf("Failed to open file: %s", err)
	}

	scanner := bufio.NewScanner(file)
	scanner.Split(bufio.ScanLines)

	var loadLines []string
	for scanner.Scan() {
		loadLines = append(loadLines, scanner.Text())
	}
	file.Close()
	return loadLines
}

// WriteProcessedLoads opens an output file and writes processed lines to it.
func WriteProcessedLoads(filename string) {
	absPathCreate, _ := filepath.Abs(filename)
	newFile, err := os.Create(absPathCreate)
	if err != nil {
		log.Fatalf("Failed to create file: %s", err)
	}

	for _, processedLine := range GetProcessedLoads() {
		byteSlice, _ := json.Marshal(processedLine)
		_, err = newFile.WriteString(fmt.Sprint(string(byteSlice)) + "\n")
		if err != nil {
			log.Printf("Failed to write line: %s", err)
		}
	}
	newFile.Close()
}

// ProcessLoads iterates over the string array of loads and processes them.
func ProcessLoads(loadLines []string) {
	for _, nextLine := range loadLines {
		var load Load
		json.Unmarshal([]byte(nextLine), &load)
		ProcessLoad(load)
	}
}

// ProcessLoad processes a load. This will take a load entry and insert it into the appropriate store and calculate vs the limits for the user.
func ProcessLoad(load Load) {
	checkForLimitReset(load)
	_, ok := loadStore[load.ID+load.CustomerID]
	if !ok {
		loadStore[load.ID+load.CustomerID] = load
		customerLimit, ok := limitStore[load.CustomerID]
		_, i := utf8.DecodeLastRuneInString(load.LoadAmount)
		loadAmountF, _ := strconv.ParseFloat(load.LoadAmount[i:], 64)
		// Check if there is a limit existing, this user will have to have limit calculations done. Else treat as their first deposit.
		if ok {
			if shouldProcessLoad(customerLimit, loadAmountF) {
				updateLimitSuccess(load, customerLimit, loadAmountF)
				updateProcessedStore(load, true)
			} else {
				updateLimitDecline(load, customerLimit)
				updateProcessedStore(load, false)
			}
		} else {
			if loadAmountF <= DailyLimit {
				limitStore[load.CustomerID] = Limit{load.CustomerID, loadAmountF, loadAmountF, 1}
				updateProcessedStore(load, true)
			} else {
				limitStore[load.CustomerID] = Limit{load.CustomerID, 0, 0, 1}
				updateProcessedStore(load, false)
			}
		}
	} else {
		log.Printf("Not processing duplicate load: %s %s", load.ID, load.CustomerID)
	}
}

// Checks all limits and returns if load should be processed
func shouldProcessLoad(customerLimit Limit, loadAmountF float64) bool {
	return customerLimit.DailyLimit+loadAmountF <= DailyLimit && customerLimit.WeeklyLimit+loadAmountF <= WeeklyLimit && customerLimit.DailyLoads < DailyLoads
}

func updateLimitSuccess(load Load, customerLimit Limit, loadAmountF float64) {
	limitStore[load.CustomerID] = Limit{load.CustomerID, customerLimit.DailyLimit + loadAmountF, customerLimit.WeeklyLimit + loadAmountF, customerLimit.DailyLoads + 1}
}

func updateLimitDecline(load Load, customerLimit Limit) {
	limitStore[load.CustomerID] = Limit{load.CustomerID, customerLimit.DailyLimit, customerLimit.WeeklyLimit, customerLimit.DailyLoads + 1}
}

func updateProcessedStore(load Load, accepted bool) {
	processedStore = append(processedStore, ProcessedLoad{load.ID, load.CustomerID, accepted})
}

// Checks if it is a new Day or new Week and resets the limits for all users if applicable.
func checkForLimitReset(load Load) {
	// Initialize limitDate for identifying a change in day
	if (time.Time{}) == limitDate {
		limitDate = time.Date(load.Time.Year(), load.Time.Month(), load.Time.Day(), 0, 0, 0, 0, time.UTC)
	}

	timeBetweenDays := load.Time.Sub(limitDate)
	oneDay := 24 * time.Hour
	hoursBetweenDays := timeBetweenDays.Truncate(oneDay).Hours()
	resetDailyLimits(load, hoursBetweenDays)
}

func resetWeeklyLimits(load Load) {
	dayOfWeek := load.Time.Weekday().String()
	if dayOfWeek == "Monday" {
		for _, limitToReset := range limitStore {
			limitStore[limitToReset.CustomerID] = Limit{limitToReset.CustomerID, limitToReset.DailyLimit, 0, limitToReset.DailyLoads}
		}
	}
}

func resetDailyLimits(load Load, hoursBetweenDays float64) {
	if hoursBetweenDays >= 24 {
		for _, limitToReset := range limitStore {
			limitStore[limitToReset.CustomerID] = Limit{limitToReset.CustomerID, 0, limitToReset.WeeklyLimit, 0}
		}
		resetWeeklyLimits(load)
		limitDate = time.Date(load.Time.Year(), load.Time.Month(), load.Time.Day(), 0, 0, 0, 0, time.UTC)
	}
}
