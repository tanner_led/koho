package main

import (
	"bytes"
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"os"
	"path/filepath"
	"testing"

	"bitbucket.org/tanner_led/koho/handlers"
	"github.com/gorilla/mux"
	"github.com/stretchr/testify/assert"
)

func TestProcessEnpoint(t *testing.T) {
	request, _ := http.NewRequest("POST", "/api/loads/process", bytes.NewBuffer(buildProcessLoadsReq("input.txt")))
	response := httptest.NewRecorder()
	Router().ServeHTTP(response, request)
	assert.Equal(t, 201, response.Code, "Created response is expected")

	//Remove output
	filename, _ := filepath.Abs("output.txt")
	os.Remove(filename)
}

func TestProcessEnpointBadRequest(t *testing.T) {
	request, _ := http.NewRequest("POST", "/api/loads/process", bytes.NewBuffer(buildProcessLoadsReq("missing.txt")))
	response := httptest.NewRecorder()
	Router().ServeHTTP(response, request)
	assert.Equal(t, 400, response.Code, "Created response is expected")
}

func Router() *mux.Router {
	router := mux.NewRouter()
	handler := handlers.NewLoads()

	router.HandleFunc("/api/loads/process", handler.ProcessLoadsHandler)
	return router
}

func buildProcessLoadsReq(file string) []byte {
	filename, _ := filepath.Abs(file)
	resp := struct {
		Filename string `json:"filename"`
	}{filename}
	byteResp, _ := json.Marshal(resp)
	return byteResp
}
