# README #

KOHO take-home assignment.

### Setup/Operation ###

* Run app normally executing the koho.exe that is generated after go build.
* App runs on localhost:8000
* Requires the following 3rd party dependencies:
    * github.com/gorilla/mux
    * github.com/stretchr/testify/assert

* POST request to localhost:8000/api/loads/process
* Req body json - {"filename": "input.txt"}

* Output will be the file destination of the output.txt file