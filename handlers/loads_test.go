package handlers

import (
	"encoding/json"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestBuildProcessResp(t *testing.T) {
	resp := buildProcessLoadsResp()
	expected := ProcessResp{"C:/Users/ttled/go/src/bitbucket.org/tanner_led/koho/handlers/output.txt"}

	var res ProcessResp
	json.Unmarshal(resp, &res)
	assert.Equal(t, expected, res, "Created response is expected")
}

func TestBuildBadReqResp(t *testing.T) {
	resp := buildBadReqResp()
	expected := ErrorResp{"Empty file or file not found!"}

	var res ErrorResp
	json.Unmarshal(resp, &res)
	assert.Equal(t, expected, res, "Created response is expected")
}
