package handlers

import (
	"encoding/json"
	"net/http"
	"path/filepath"

	"bitbucket.org/tanner_led/koho/data"
)

// const for output name
const (
	OutputFilename = "output.txt"
)

// ProcessResp is the struct type for ProcessLoadsHandler response
type ProcessResp struct{
	FileDestination string `json:"file_destination"`
}

// ErrorResp error response struct
type ErrorResp struct {
	Error string `json:"error"`
}

// LoadsHandler is a http.handler
type LoadsHandler struct{}

// NewLoads creates a load handler
func NewLoads() *LoadsHandler {
	return &LoadsHandler{}
}

// ProcessLoadsHandler processes the given file destination if it is a file containing json loads
func (b *LoadsHandler) ProcessLoadsHandler(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "applcation/json")
	loadLines := data.ReadLoads(r)
	if len(loadLines) == 0 {
		w.WriteHeader(http.StatusBadRequest)
		w.Write(buildBadReqResp())
	} else {
		data.ProcessLoads(loadLines)
		data.WriteProcessedLoads(OutputFilename)

		w.WriteHeader(http.StatusCreated)
		w.Write(buildProcessLoadsResp())
	}
}

func buildProcessLoadsResp() []byte {
	fileOutputPath, _ := filepath.Abs(OutputFilename)
	fileOutputPath = filepath.ToSlash(fileOutputPath)
	resp := ProcessResp{fileOutputPath}
	byteResp, _ := json.Marshal(resp)
	return byteResp
}

func buildBadReqResp() []byte {
	resp := ErrorResp{"Empty file or file not found!"}
	byteResp, _ := json.Marshal(resp)
	return byteResp
}
