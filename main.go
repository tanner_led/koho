package main

import (
	"log"
	"net/http"

	"bitbucket.org/tanner_led/koho/handlers"
	"github.com/gorilla/mux"
)

func main() {

	//Init handler and router
	handler := handlers.NewLoads()
	router := mux.NewRouter()

	getRouter := router.Methods(http.MethodPost).Subrouter()
	getRouter.HandleFunc("/api/loads/process", handler.ProcessLoadsHandler)

	log.Fatal(http.ListenAndServe(":8000", router))
}
